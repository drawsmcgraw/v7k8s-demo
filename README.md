# vSphere with Tanzu Demo Repo

## Overview

This repo contains the files necessary to deliver a basic demo of vSphere with Tanzu, including creation of Namespaces, handoff to developer, and deploying your first k8s workload.

To see a video of the demo, see [this video on Youtube](https://www.youtube.com/watch?v=9z_zAQIjRy0)

The intention of this repo (and the demo script outlined below) is to showcase the simple use of a VI admin using vCenter to deliver Kubernetes access to a developer, followed by showing how a developer creates a Kubernetes cluster and deploys a simple workload, including a load balancer

## About these files

All files are normal Kubernetes manifest files meant to be used with Tanzu Kubernetes Grid.

For example, creating a Kubernetes cluster would look like this:
```
kubectl apply -f a-team-k8s-cluster.yml
```


**The only exception** is `homelab-wcp.yml` and files with the word `namespace` in them (e.g. `a-team-namespace.yml`. These files are meant to be used in conjunction with [wcpctl](https://github.com/papivot/wcpctl) to enable Workload Management on a vSphere installation and to create Tanzu Namespaces, respectively. Usage looks like this:

```
wcpctl.py create homelab-wcp.yml
wcpctl.py create a-team-namespace.yml
```

This is a very light, high-level example. The examples above assume you have already authenticated to the appropriate systems (vSphere, Kubernetes, etc).



## A note on Pod Security Policies
Due to the Secure by Default configuration of Tanzu Kubernetes Clusters, you'll need to create a clusterRoleBinding in order to deploy complex workloads. The easiest way to accomplish this is to issue the below command on a freshly-built k8s cluster:

```
kubectl create clusterrolebinding psp:authenticated --clusterrole=psp:vmware-system-privileged --group=system:authenticated 
```


# Demo Steps

At a high level, the demo is as follows:
- (Before the customer demo starts) - Create the A-Team namespace and k8s cluster.
- (Before the customer demo starts) - Test the a-team k8s cluster by deploying the sample workload and viewing it in a browser.
- (Before the customer demo starts) - Create users and groups to be used in the demo (details below)
- Show native k8s components in the vCenter UI, using the a-team namespace and k8s cluster as reference
- Use the vCenter UI to create the B-Team namespace, show how this is done, including configuring permissions/storage/quota
- Use the new namespace to make a k8s cluster
- Deploy a simple workload onto the new k8s cluster

## Prep Work (Before the Customer Demo)

## Step 1 - Create users in your IDAM

This can be in Active Directory, the `vsphere.local` built-in SSO, whatever is the right IDAM for your demo.

```
users:
  - a-user
  - b-user

groups:
  - a-team:
    members:
      - a-user
  - b-team:
    members:
      - b-user
```


## Step 2 - Modify the k8s manifest files to suit your environment

The files `a-team-k8s-cluster.yml` and `b-team-k8s-cluster.yml` are heavily commented and have guidelines to help you know what needs to be modified and how to find said detail. You will be using these later, so having the correct values is required.

## Step 3 - Create the 'A-Team' namespace and Kubernetes cluster

The demo involves showing k8s cluster creation by creating the b-team k8s cluster. But we want to have another k8s cluster ready to go so we don't have to wait on the b-team k8s cluster. That cluster will be the a-team cluster.

- Use vCenter UI to create a namespace, `a-team`. 
- Assign the `a-team` the `edit` permissions.
- Assign the appropriate storage

After creating the namespace, use the link in vCenter (in the Namespace view panel) to download `kubectl` and the `kubectl-vsphere` plugin. Place them on your $PATH. Typically for a Linux workstation, this means placing the binaries into `/usr/local/bin/.`.

For MacOS, use the following to add them to your $PATH:
```
# Make the binaries executable. 
chmod +x kubectl
chmod +x kubectl-vsphere

# Move the binaries to a file location on your system PATH 
sudo mv kubectl /usr/local/bin/.
sudo mv kubectl-vsphere /usr/local/bin/.

# Test to ensure the version you installed is up-to-date
kubectl version --client
```

Log into the a-team Namespace and create a k8s cluster.
```
# Log into the a-team namespace as the a-user.
# Use the value for your $WCP_SERVER from your environment.
kubectl vsphere login --vsphere-username a-user@mgsddc.lab --server=$WCP_SERVER --insecure-skip-tls-verify

# Confirm you've switched to the correct Kubernetes context (i.e. the 'a-team' namespace)
kubectl config use-context a-team

# Deploy the a-team k8s cluster
kubectl apply -f a-team-k8s-cluster.yml
```

## Step 4 - Test the a-team k8s Cluster

Confirm the cluster creation has finished with `kubectl`:
```
kubectl get TanzuKubernetesClusters
NAME                    CONTROL PLANE   WORKER   DISTRIBUTION                     AGE   PHASE
a-team-k8s-cluster      1               1        v1.18.5+vmware.1-tkg.1.c40d30d   2d    running
```

Once confirmed, you need to log into the cluster itself. Previously, you only logged into the Namespace. You now need to log into the k8s cluster itself. This is an artifact of the Role of Separation - imagine a Devops team who creates clusters, and a development team who logs into them.

Create the role-binding to allow nontrivial workloads. Do not skip this step, otherwise your deployment will hang indefinitely with no useful errors.
```
# Log into the newly-created k8s cluster
kubectl vsphere login --vsphere-username a-user@mgsddc.lab --server=$WCP_SERVER --insecure-skip-tls-verify --tanzu-kubernetes-cluster-name=a-team-k8s-cluster

# Create the role-binding
kubectl create clusterrolebinding psp:authenticated --clusterrole=psp:vmware-system-privileged --group=system:authenticated 

# Deploy a simple workload:
kubectl apply -f hello-k8s.yml
```

Wait a moment (or several if this is the very first workload on your WCP cluster), then fetch the external IP for your workload. You may see multiple entries, but the one that's important is the one for your `hello-k8s` cluster:
```
kubectl get svc
NAME         TYPE           CLUSTER-IP      EXTERNAL-IP      PORT(S)        AGE
hello-k8s    LoadBalancer   10.100.77.188   192.168.11.166   80:31742/TCP   9d
```

In this case, the IP you want is 192.168.11.166

Plug that IP into a browser and test. You should see your workload up.

You have now completed the "pre-demo" section of work.

## Live Demo Instructions

This is largely a repeat of what you did in the "pre-demo" section, which has lots of explanations. For the sake of brevity and tactical situations, this section will be largely absent any explanations and contain mostly commands you can copy/paste.

The only difference is that, in the "pre-demo" section, we were working with the "a-team" namespace. During the live demo, we'll be working mostly with the "b-team" namespace.

## Step 1 - Show the `Namespaces` namespace in vCenter

Explain that there is a special namespace, called `Namespaces` that houses all k8s objects - user namespaces, k8s clusters, VMs, etc. Show whatever is relevant to the customer - storage backend, permissions, quotas, monitoring, etc.

## Step 2 - Use the vCenter UI to create a new namespace

Just like you did in the "pre-demo" section, including assigning permissions. Be sure to TAKE YOUR TIME and MOVE SLOWLY. The customer has never seen this stuff before, so be sure they follow what you're doing and that they understand HOW EASY THIS IS.

## Step 3 - Log in as a developer and create a k8s cluster.

```
# Log in as a developer
kubectl vsphere login --vsphere-username b-user@mgsddc.lab --server=$WCP_SERVER --insecure-skip-tls-verify

# Switch context
kubectl config use-context b-team

# Create the k8s cluster
kubectl apply -f b-team-k8s-cluster.yml
```

That first command (the `kubectl vsphere login`) is a *huge differentiator* for us! Kubernetes, on its own, does not manage user accounts - you must implement that yourself. With vSphere with Kubernetes, however, _you can use your existing IDAM solution_ without any extra work. This is _huge_ for operators and you should emphasize that user management is "built in".

Show that VMs are now being created in response to the request.

Explain that this is a regular Kubernetes manifest file, just like you would deploy a 'pod' or a 'deployment'. This is due to the fact that vSphere with Tanzu uses _ClusterAPI_, which allows us to, in a declarative fashion, describe the type of k8s clusters we need, and ClusterAPI takes care of the rest. This is why we can deploy to vSphere, AWS, Azure, GCP, etc _in the exact same way_ (i.e. with the same manifest file). 

## Step 4 - Switch to the A-Team k8s cluster and deploy onto there.

Because we don't want to make the audience wait while the b-team k8s cluster finishes building, we're going to use the "baking show" approach and switch over to the a-team k8s cluster, which is already done.

```
# Log into the a-team k8s cluster. NOTE the use of the '--tanzu-kubernetes-cluster-name'
kubectl vsphere login --vsphere-username a-user@mgsddc.lab --server=$WCP_SERVER --tanzu-kubernetes-cluster-name=a-team-k8s-cluster --insecure-skip-tls-verify

# Switch context to this cluster
kubectl config use-context a-team-k8s-cluster

# Deploy the simple workload
kubectl apply -f hello-k8s.yml

# Fetch the external IP
kubectl get svc
```

Use the external IP to show that the workload has been deployed.

## Step 5 - Explanation and Wrap-up

Be sure the audience understands the following:
- The VI operator _did not_ need to learn anything new. They stayed in vCenter and created all the necessary objects. 
- Everything the VI operator did is driven by APIs. So if you have VRA, ServiceNow, etc, _you can integrate Tanzu with your existing automation_.
- The developer (who made the k8s cluster) did not have to write any automation. All they did was apply a simple text file and they got their custom k8s cluster.
- All Tanzu Kubernetes clusters are _100%_ open source. We do not add anything downstream that slows down releases. We do not have proprietary code we keep secret.

This concludes the written part of the script. 
